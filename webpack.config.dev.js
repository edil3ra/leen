const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var requireYml = require('require-yml')


module.exports = {
  target: "web",
  mode: 'development',
  
  entry: path.join(__dirname, '/src/app/index.js'),

  output: {
	path: path.resolve(__dirname, 'dist'),
	filename: '[name].js'
  },

  devServer: {
	port: 9000, // port to run dev-server
	contentBase: [
      path.join(__dirname, 'src/templates'),
      path.join(__dirname, 'src/data')
    ],
	watchContentBase: true,
    watchOptions: {
      poll: true
	}
  },

  devtool: 'source-map',

  module: {
    rules: [
	  {
		test: /\.js$/,
		exclude: /(node_modules|bower_components)/,
		use: [{
		  loader: 'babel-loader',
		  options: {
            presets: ['@babel/preset-env']
          }
		}
		]},
      { 
        test: /\.hbs$/,
        loader: "handlebars-loader",
        options: {
          helperDirs: path.join(__dirname, 'modules/helpers'),
          precompileOptions: {
            knownHelpersOnly: false,
          },
        }
      },
	  {
		test: /\.(sa|sc|c)ss$/,
		use: [
		  MiniCssExtractPlugin.loader,
		  'css-loader',
		  'postcss-loader',
		  'sass-loader',
		],
	  },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/'
          }
        }]
      },
	  {
		test: /\.(jpe?g|png|gHif|svg|tga|gltf|babylon|mtl|pcb|pcd|prwm|obj|mat|mp3|ogg)$/i,
		use: [
		  {
			loader: 'file-loader',
			options: {
			  name: "assets/[name].[ext]"
			},
		  }
		]
	  }	
	]},

  optimization: {
	minimize: false
  },
  plugins: [
	new HtmlWebpackPlugin({
      title: 'Leen',
	  template: __dirname + "/src/templates/index.hbs",
      templateParameters: {
        // data: require(__dirname + '/src/data/data.js'),
        data: requireYml(__dirname + '/src/data/data.yaml')
      },
	  inject: 'body',
	  favicon: './src/assets/images/favicon-32.png',

	}),
	new webpack.ProvidePlugin({
	  $: 'jquery',
	  jQuery: 'jquery',
	}),

	new MiniCssExtractPlugin({
	  filename: '[name].css',
	  chunkFilename: '[id].css'
	})
  ]
  
};
