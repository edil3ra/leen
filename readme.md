# install
    npm install
# run in developement
    npm start
    localhost:9000
# build the site
    npm run build
# deploy
    `The website is build in the dist folder, if you want to deploy with ftp client all you need to do is copy the dist folder into the correct location, for us it's in the racine, be aware that *.js and *.css names changed with each build, so you probably need to clean thoses files when deploying, the easist way is to delete everyithng on the server copy paste the dist folder in root directory `
##  note
    `Deploying  is easier with access to ssh, a run command in the package.json.
     Repo should be protected if so.
     Right now there is a hook on each push on the master so you can preview it at: keen-payne-68278a.netlify.com
     Probably bad for seo to have duplicate site so it will be removed
     `
# text changing
    almost all the text can be change in the data src/data/data.yaml file
    `be aware than in developing mode the change is not catch by webpack, so you need to restart the server to see the new text, npm start again`
    
# images
## site
 The easist way to change image is to copy the new images for the section choosen but keep the same name, if you change the name you need to go to index.js and adpat the name for each section name, you also need it to do in the data.yaml file, so don't bother just copy the file into the correct folder with the correct names
 
### biographie images
* import '../assets/images/biographie/leen1.jpg'
* import '../assets/images/biographie/leen2.jpg'
* import '../assets/images/biographie/leen3.jpg'
* import '../assets/images/biographie/book.jpg'
* import '../assets/images/biographie/book2.jpg'

### practise images 
* import '../assets/images/practise/room1.jpg'
* import '../assets/images/practise/room2.jpg'

### year images
* import '../assets/images/year/year1.jpg'
* import '../assets/images/year/year2.jpg'
* import '../assets/images/year/year3.jpg'

### workshops images
* import '../assets/images/workshops/workshop1.jpg'
* import '../assets/images/workshops/workshop2.jpg'
* import '../assets/images/workshops/workshop3.jpg'
* import '../assets/images/workshops/workshop4.jpg'

 
## background
goto: src/assets/style/Add images into background folder and change the name for the approriate section 

