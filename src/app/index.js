import '../assets/style/foundation.scss'
import '../assets/style/app.scss'
import '../assets/style/icons/foundation-icons.css'
// Either import everything
// import 'foundation-sites'

// Or import individual modules
import 'foundation-sites/dist/js/plugins/foundation.core'
import 'foundation-sites/dist/js/plugins/foundation.dropdownMenu.min.js'
import 'foundation-sites/dist/js/plugins/foundation.sticky.min.js'
import 'foundation-sites/dist/js/plugins/foundation.orbit.min.js'
import 'foundation-sites/dist/js/plugins/foundation.util.keyboard.min.js'
import 'foundation-sites/dist/js/plugins/foundation.util.box.min.js'
import 'foundation-sites/dist/js/plugins/foundation.util.motion.min.js'
import 'foundation-sites/dist/js/plugins/foundation.util.nest.min.js'


// biographie images
import '../assets/images/biographie/leen1.jpg'
import '../assets/images/biographie/leen2.jpg'
import '../assets/images/biographie/leen3.jpg'
import '../assets/images/biographie/book.jpg'
import '../assets/images/biographie/book2.jpg'

// practise images
import '../assets/images/practise/room1.jpg'
import '../assets/images/practise/room2.jpg'

// year images
import '../assets/images/year/year1.jpg'
import '../assets/images/year/year2.jpg'
import '../assets/images/year/year3.jpg'

// workshops images
import '../assets/images/workshops/workshop1.jpg'
import '../assets/images/workshops/workshop2.jpg'
import '../assets/images/workshops/workshop3.jpg'
import '../assets/images/workshops/workshop4.jpg'


function scrollHash(e) {
  e.preventDefault()
  var target = $(this).attr("href")

  $('html, body').stop().animate({
	scrollTop: $(target).offset().top - 50
  }, 600, function() {
    if(history.pushState) {
      history.pushState(null, null, target)
    }
    else {
      location.hash = target
    }
  })
  return false;  
}

document.addEventListener("DOMContentLoaded", function(){
  $(function(){ $(document).foundation() })
  $('#my-menu a').addClass("underline-from-center")

  $('#my-menu a').click(scrollHash)
  $('.bottom-content-section a').click(scrollHash)

  $(window).scroll(function() {
    var scrollDistance = $(window).scrollTop()
    $('.page-section').each(function() {
      const id = $(this).attr('id')
      if ($(this).position().top <= scrollDistance + 51) {
        $('#my-menu a').addClass("underline-from-center")
    	$('.navigation li.active').removeClass('active')
    	$(`.navigation a[href$="${id}"]`).parent().addClass('active')
        $(`.navigation a[href$="${id}"]`).removeClass("underline-from-center")
      }
    })
  }).scroll()


})



