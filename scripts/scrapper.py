from pathlib import Path
import logging

import requests
from bs4 import BeautifulSoup

DATA_FOLDER = Path('./data')
HTML_FOLDER = DATA_FOLDER / 'html'
IMAGE_FOLDER = DATA_FOLDER / 'images'
TEXT_FOLDER = DATA_FOLDER / 'texts'
logger = logging.getLogger('scrapping')
logger.setLevel(logging.INFO)
hostname = 'http://www.therapytime.be'

links = [{
    'name': 'home',
    'link': hostname
}, {
    'name': 'home',
    'link': f'{hostname}/praktijk.htm'
}, {
    'name': 'jaartraining',
    'link': f'{hostname}/jaartraining.htm'
}, {
    'name': 'workshops',
    'link': f'{hostname}/workshops.htm'
}, {
    'name': 'agenda',
    'link': f'{hostname}/agenda.htm'
}, {
    'name': 'biografie',
    'link': f'{hostname}/biografie.htm'
}, {
    'name': 'info',
    'link': f'{hostname}/info.htm'
}, {
    'name': 'literatuur',
    'link': f'{hostname}/literatuur.htm'
}, {
    'name': 'links',
    'link': f'{hostname}/links.html'
}]


def download_html(name, link, folder=DATA_FOLDER):
    filename = folder / f'{name}.html'
    response = requests.get(link)
    with open(filename, 'w') as f:
        f.write(response.text)


def download_image(name, link, folder=DATA_FOLDER):
    filename = folder / f'{name}'
    response = requests.get(link)
    with open(filename, 'wb') as f:
        f.write(response.content)


def create_text_from_html(name, html, folder=DATA_FOLDER):
    text = BeautifulSoup(html).text
    filename = folder / f'{name}.txt'
    with open(filename, 'w') as f:
        f.write(text)


def create_text_from_html2(name, html, folder=DATA_FOLDER):
    text = BeautifulSoup(html).text
    text_split = text.split('terug')
    
    if len(text_split) > 1:
        text2 = text_split[1].strip()
        filename = folder / f'{name}.txt'
        with open(filename, 'w') as f:
            f.write(text2)
        

def download_images(html):
    images = BeautifulSoup(html).select('img')
    for image in images:
        src = image['src']
        link = f'{hostname}/{src}'
        filename = IMAGE_FOLDER / src

        if filename.exists():
            logger.info(f'filename: {filename.name} already exist')
        else:
            logger.info(f'start downloading image to {filename.name}')
            download_image(folder=IMAGE_FOLDER, name=src, link=link)
            logger.info(f'start downloading image to {filename.name}')


def download_htmls(links):
    HTML_FOLDER = DATA_FOLDER / 'html'
    HTML_FOLDER.mkdir(exist_ok=True)

    for link in links:
        filename = HTML_FOLDER / f'{link["name"]}.html'
        if filename.exists():
            logger.info(f'filename: {filename.name} already exist')
        else:
            logger.info(f'start downloading {filename.name}')
            download_html(folder=HTML_FOLDER, **link)
            logger.info(f'end downloading {filename.name}')


def main():
    download_htmls(links)

    raw_htmls = [{
        'name': filename.name,
        'filename': filename,
        'html': open(filename).read()
    } for filename in HTML_FOLDER.iterdir()]

    for html in raw_htmls:
        download_images(html['html'])

    for html in raw_htmls:
        create_text_from_html(html['name'].split('.')[0], html['html'], TEXT_FOLDER)
        create_text_from_html2(html['name'].split('.')[0] + '_2', html['html'], TEXT_FOLDER)

    raw_html = raw_htmls[0]
