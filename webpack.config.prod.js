const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin')
const ImageminPlugin = require("imagemin-webpack");
 
// Before importing imagemin plugin make sure you add it in `package.json` (`dependencies`) and install
const imageminGifsicle = require("imagemin-gifsicle");
const imageminJpegtran = require("imagemin-jpegtran");
const imageminOptipng = require("imagemin-optipng");
const imageminSvgo = require("imagemin-svgo");
var requireYml = require('require-yml')

module.exports = {
  target: "web",
  mode: 'production',

  entry: path.join(__dirname, '/src/app/index.js'),

  output: {
	filename: '[hash].bundle.js'
  },

  module: {  // where we defined file patterns and their loaders
    rules: [
	  {
		test: /\.js$/,
		exclude: /(node_modules|bower_components)/,
		use: [{
		  loader: 'babel-loader',
		  options: {
            presets: ['@babel/preset-env']
          }
		}
		]},
      { 
        test: /\.hbs$/,
        loader: "handlebars-loader",
        options: {
          helperDirs: path.join(__dirname, 'modules/helpers'),
          precompileOptions: {
            knownHelpersOnly: false,
          },
        }
      },
	  {
		test: /\.(sa|sc|c)ss$/,
		use: [
		  MiniCssExtractPlugin.loader,
		  'css-loader',
		  'postcss-loader',
		  'sass-loader',
		],
	  },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/'
          }
        }]
      },
	  {
		test: /\.(jpe?g|png|gif|svg|tga|gltf|babylon|mtl|pcb|pcd|prwm|obj|mat|mp3|ogg)$/i,
		use: [
		  {
			loader: 'file-loader',
			options: {
			  name: "assets/[name].[ext]"
			},
		  }
		]
	  }	
	]},

  resolve: {
	alias: {
	  jquery: "jquery/dist/jquery",
	  index: path.resolve(__dirname, 'src/app/index.js')
	}
  },

  optimization: {
	minimizer: [
	  new UglifyJsPlugin({
		cache: true,
		parallel: true,
		sourceMap: false
	  }),
	  new OptimizeCSSAssetsPlugin({})
	]
  },
  plugins: [
	new CleanWebpackPlugin(['dist']),
    new ImageminPlugin({
      bail: false,
      cache: true,
      imageminOptions: {
        plugins: [
          imageminGifsicle({
            interlaced: true
          }),
          imageminJpegtran({
            progressive: true
          }),
          imageminOptipng({
            optimizationLevel: 5
          }),
          imageminSvgo({
            removeViewBox: true
          })
        ]
      }
    }),
	new HtmlWebpackPlugin({
      title: 'Leen',
	  template: __dirname + "/src/templates/index.hbs",
	  filename: 'index.html',
      templateParameters: {
        data: requireYml(__dirname + '/src/data/data.yaml')
      },
	  inject: 'body',
	  favicon: './src/assets/images/favicon-32.png'
	}),
	new webpack.ProvidePlugin({
	  $: 'jquery',
	  jQuery: 'jquery',
	}),

	new MiniCssExtractPlugin({
	  filename: '[hash].css',
	  chunkFilename: '[id].css'
	})
  ]
  
};
